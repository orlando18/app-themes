export interface Theme {
    background: string,
    text: string,
    border: string,
    primary: string,
    secondary: string
}