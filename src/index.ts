import { Theme } from './interfaces';

const dark: Theme = {
    background: '#000000',
    text: '#FFFFFF',
    border: '#000000',
    primary: '#FF0000',
    secondary: '#00FF00',
};

const light: Theme = {
    background: '#FFFFFF',
    text: '#000000',
    border: '#000000',
    primary: '#FF0000',
    secondary: '#00FF00',
};

export const orgThemes = {
    dark,
    light,
};
